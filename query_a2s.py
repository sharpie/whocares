import a2s
from   misc import *

# Get campaign name from mapname
def	get_campaign(info):
	map_name = info.map_name
	if info.game == "Left 4 Dead 2":
		if map_name.startswith("c1m"):
			return "Dead Center"
		if map_name.startswith("c2m"):
			return "Dark Carnival"
		if map_name.startswith("c3m"):
			return "Swamp Fever"
		if map_name.startswith("c4m"):
			return "Hard Rain"
		if map_name.startswith("c5m"):
			return "The Parish"
		if map_name.startswith("c6m"):
			return "The Passing"
		if map_name.startswith("c7m"):
			return "The Sacrifice"
		if map_name.startswith("c8m"):
			return "No Mercy"
		if map_name.startswith("c9m"):
			return "Crash Course"
		if map_name.startswith("c10m"):
			return "Death Toll"
		if map_name.startswith("c11m"):
			return "Dead Air"
		if map_name.startswith("c12m"):
			return "Blood Harvest"
		if map_name.startswith("c13m"):
			return "Cold Stream"
		if map_name.startswith("c14m"):
			return "The Last Stand"
		return f"Custom ({map_name})"
	else:
		return map_name

async def query_a2s(q):
	try:
		server = (q['ip'], q['port'])
		print(f"query_a2s: server: {server}")
		timeout = q['timeout']
		info = await a2s.ainfo(server, timeout)
		q['game']         = info.game
		q['name']         = info.server_name
		q['map']          = info.map_name
		q['campaign']     = get_campaign(info)
		q['max_players']  = info.max_players
		q['player_count'] = info.player_count
		try:
			players = await a2s.aplayers(server, timeout)
			print(f"query_a2s: players returned: {players}")
			q['players'] = list(map(lambda p : p.name, players))
		except Exception as ex:
			ex_str = get_ex_str(ex)
			print(f"querya2s:aplayers: ex: {ex_str}")
			q['players'] = [ ex_str ]

	except Exception as ex:
		ex_str = get_ex_str(ex)
		print(f"query a2s.ainfo: ex: {ex_str}")
		q['ex'] = ex_str

	print("query: ok")
	return q
