
class Config:

	def __init__(self, path):

		self.servers = {}
		self.token   = None
		self.timeout = 5
		self.player_count_channel_id = ""

		with open(path) as f:
			lines = f.readlines()
			lines = map(str.rstrip, lines)

		section = None

		for line in lines:
			if len(line) and not line.startswith("#"):

				if line.startswith('['):
					section = line[1:-1]
					print(f"section: {section}")
					if section not in self.servers:
						self.servers[section] = []

				elif section is None:
					k, v = line.split('=', 1)
					a = getattr(self, k)
					if isinstance(a, int):
						v = int(v)
					if isinstance(a, float):
						v = float(v)
					print(f"config: setting attr '{k}' with value {v} : {type(v)}")
					setattr(self, k, v)

				else:
					e = line.split(':')
					if len(e) == 2:
						server = (e[0], int(e[1]), "A2S")
					elif len(e) == 3:
						server = (e[0], int(e[1]), e[2])
					print(f"config: adding server: {server}")
					self.servers[section].append(server)

