#!/usr/bin/env python3

import sys

import disnake
from   disnake.ext import tasks, commands
from   emoji       import emojize

from   query  import query
from   config import Config

if len(sys.argv) < 2:
	print("Usage: whocares.py CONFIG")
	sys.exit(1)


config  = Config(sys.argv[1])


async def servers_common(servers, *, message=True, count_players=False, errors=False):
	player_count = 0
	try:
		msg = "\n```\n"
		results = await query(servers, timeout=config.timeout)
		for result in results:
			print(f"/servers:   processing result: {result}")
			endpoint = f"{result['ip']}:{result['port']}"
			content  = f"{endpoint:28}"
			if 'ex' not in result:
				player_count += result['player_count']
				result['name'] = result['name'][:29]
				content += "{name:30s}{campaign:36s}{player_count: 2d}/{max_players:02d}\n".format_map(result)
				if len(result['players']):
					for p in result['players']:
						#p = p.replace("`", "'")
						if len(p):
							content += f"  {p}\n"
			else:
				content += result['ex']
			msg += f"{content}\n"
		msg += "```"
		print("/servers: ok")
	except Exception as ex:
		print(f"/servers: ex: {type(ex)} {ex}")
		msg = f"```Exception: {type(ex)} {ex}```"

	print(f"/servers: end")

	r = []
	if message:
		r.append(msg)
	if count_players:
		r.append(player_count)
	if errors:
		errors = []
		for result in results:
			if 'ex' in result:
				errors.append(result['ex'])
			else:
				errors.append(None)
		r.append(errors)
	return tuple(r)


class Bot(commands.InteractionBot):

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.warning = ' ' + emojize(':warning:')

	@tasks.loop(minutes=5)
	async def update_player_count_task(self):
		print(f"==================================================")
		print(f"update_player_count_task: tick")
		try:
			count_players = len(config.player_count_channel_id) > 0
			if count_players:
				player_count, errors = await servers_common(config.servers["L4D2"], message=False, count_players=True, errors=True)
				print(f"update_player_count_task: active players: {player_count}")
				for channel_id in config.player_count_channel_id.split(','):
					print(f"update_player_count_task: getting channel with id: {channel_id}")
					channel = self.get_channel(int(channel_id))
					print(f"update_player_count_task: got channel '{channel}'")
					warning = ""
					if None not in errors:
						warning = self.warning
					await channel.edit(name=f"L4D2: {player_count}{warning}")
					print(f"update_player_count_task: edit channel completed")
		except Exception as ex:
			print(f"update_player_count_task: ex: {ex}")

bot = Bot()


@bot.slash_command(description="Query the WhoCares L4D2 servers")
async def servers(context):
	print(f"==================================================")
	print(f"/servers: begin: user:{context.author.name}")
	await context.response.defer(with_message=False, ephemeral=False)
	msg, = await servers_common(config.servers["L4D2"])
	await context.edit_original_message(msg)

@bot.slash_command(description="Query the other WhoCares servers")
async def servers2(context):
	print(f"==================================================")
	print(f"/servers2: begin: user:{context.author.name}")
	await context.response.defer(with_message=False, ephemeral=False)
	msg, = await servers_common(config.servers["Other"])
	await context.edit_original_message(msg)

@bot.event
async def on_ready():
	print("on_ready: begin")
	await bot.wait_until_ready()
	bot.update_player_count_task.start()
	print("on_ready: end")

bot.run(config.token)
