import asyncio
from   datetime  import datetime, timezone

from   query_a2s import query_a2s
from   query_mc  import query_mc


async def query(servers, *, timeout):
	"""
	servers: a sequence of 3-tuples (ip, port, protocol)
	timeout: timeout in ...
	"""

	def get_task(q):
		protocol = q['protocol']
		if   protocol == "A2S":
			func = query_a2s
		elif protocol == "MC":
			func = query_mc
		return func(q)

	tasks = []
	now = datetime.now(timezone.utc)

	for server in servers:
		q = {}
		q['datetime'] = now
		q['ip']       = server[0]
		q['port']     = server[1]
		q['protocol'] = server[2]
		q['timeout']  = timeout
		task = get_task(q)
		tasks.append(task)

	return await asyncio.gather(*tasks)
